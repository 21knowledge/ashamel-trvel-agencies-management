@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bravo-booking-page padding-content" >
        <div class="container">
            <div class="row booking-success-notice">
                <div class="col-lg-8 col-md-8">
                    <div class="d-flex align-items-center">
                        <img src="{{url('images/ico_success.svg')}}" alt="Payment Success">
                        <div class="notice-success">
                            <p class="line1">
                                {{__('your order was submitted successfully for:')}}
                            </p>
                            <p class="line2">{!! $booking->clients !!}</p>
                            <p class="line2">{{__('Booking details has been sent to:')}} <span>{{$booking->email}}</span></p>
                            @if($note = $gateway->getOption("payment_note"))
                                <div class="line2">{!! clean($note) !!}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <ul class="booking-info-detail">
                        <li><span>{{__('Booking Number')}}:</span> {{$booking->id}}</li>
                        <li><span>{{__('Booking Date')}}:</span> {{display_date($booking->created_at)}}</li>
                        @if(!empty($gateway))
                        <li><span>{{__('Payment Method')}}:</span> {{$gateway->name}}</li>
                        @endif
                        <li><span>{{__('Booking Status')}}:</span> {{ $booking->status_name }}</li>
                    </ul>
                </div>
            </div>
            <div class="row booking-success-detail">
                <div class="col-md-8">
                    @include ($service->booking_customer_info_file ?? 'Booking::frontend/booking/booking-customer-info')
                    {{-- <a href="{{route('user.booking.ticket',['code'=>$booking->code])}}" class="btn btn-xs btn-primary btn-info-booking open-new-window mt-1" onclick="window.open(this.href); return false;">
                        <i class="fa fa-print"></i> {{__("Print Ticket")}}
                    </a> --}}
                    <a href="{{route('user.booking.invoice',['code'=>$booking->code])}}" class="btn btn-xs btn-primary btn-info-booking open-new-window mt-1" onclick="window.open(this.href); return false;">
                        <i class="fa fa-print"></i> {{__("Invoice")}}
                    </a>
                </div>
                <div class="col-md-4">
                    @include ($service->checkout_booking_detail_file ?? '')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
@endsection
