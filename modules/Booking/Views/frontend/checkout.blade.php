@extends('layouts.app',['container_class'=>'container-fluid','header_right_menu'=>true])
@section('head')
<link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('dist/frontend/module/tour/css/tour.css?_ver='.config('app.version')) }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    <style type="text/css">
        .bravo_topbar, .bravo_footer {
            display: none
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
@endsection
@section('content')
    <div class="bravo-booking-page padding-content" >
        <div class="container">
            <div id="bravo-checkout-page" >
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="form-title">{{__('Booking Submission')}}</h3>
                         <div class="booking-form">
                             @include ($service->checkout_form_file ?? 'Booking::frontend/booking/checkout-form')
                             
                         </div>
                    </div>
                    <div class="col-md-4">
                        <div class="booking-detail booking-form"> 
                            @include ($service->checkout_booking_detail_file ?? '')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<div class="loaderContainer"><div class="loader"></div></div> 
    <script src="{{ asset('module/booking/js/checkout.js') }}"></script>
    <script type="text/javascript">
        jQuery(function () {
            $.ajax({
                'url': bookingCore.url + '{{$is_api ? '/api' : ''}}/booking/{{$booking->code}}/check-status',
                'cache': false,
                'type': 'GET',
                success: function (data) {
                    if (data.redirect !== undefined && data.redirect) {
                        window.location.href = data.redirect
                    }
                }
            });
        })

        $('.deposit_amount').on('change', function(){
            checkPaynow();
        });

        $('input[type=radio][name=how_to_pay]').on('change', function(){
            checkPaynow();
        });

        function checkPaynow(){
            var credit_input = $('.deposit_amount').val();
            var how_to_pay = $("input[name=how_to_pay]:checked").val();
            var convert_to_money = credit_input * {{ setting_item('wallet_credit_exchange_rate',1)}};

            if(how_to_pay == 'full'){
                var pay_now_need_pay = parseFloat({{floatval($booking->total)}}) - convert_to_money;
            }else{
                var pay_now_need_pay = parseFloat({{floatval($booking->deposit == null ? $booking->total : $booking->deposit)}}) - convert_to_money;
            }

            if(pay_now_need_pay < 0){
                pay_now_need_pay = 0;
            }
            $('.convert_pay_now').html(bravo_format_money(pay_now_need_pay));
            $('.convert_deposit_amount').html(bravo_format_money(convert_to_money));
        }
    </script>
    <style>
        .booking-detail.booking-form {
            position: fixed;
            width: 400px;
        }
        .row.boxed {
            border: 3px solid #7cb9c494;
            margin: 10px;
            padding: 10px;
            padding-top: 10px;
            padding-top: 29px;
            background-color: aliceblue;
            border-radius: 13px;
        }
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            display: block;
            position: absolute;
            top: 40%;
            left: 40%;
        }
        .loaderContainer{
            width: 100%;
            height: 100%;
            position: fixed;
            top:0px;
            left: 0px;
            background-color: #7578793a;
            display: none;
        }

        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }
    </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.hotels').change(function(){
                    

                    let currentID = $(this).attr('currIndex');
                    let varSelectedIndex = $(this).val();

                    $('.loaderContainer').css('display', 'block');

                    //alert(varSelectedIndex)
                    //element.title + '(MAD ' + parseFloat(element.price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() +')'

                    $.ajax({
                        'url': bookingCore.url + '{{$is_api ? '/api' : ''}}/booking/{{$booking->code}}/get-rooms?id=' + varSelectedIndex + '&code={{$booking->code}}',
                        'cache': false,
                        'type': 'GET',
                        success: function (data) {
                            let rooms = JSON.parse(data)
                            let comboBox = $('#rooms_' + currentID);
                            comboBox.empty();
                            rooms.forEach(element => {
                                console.log(element.title)
                                var option = $('<option />');
                                option.attr('value', element.id).text(element.title + ' F.Pls: ' + element.leftPlaces + '/' + element.totalPlaces);
                                if(element.leftPlaces == 0){
                                    option.attr('disabled', 'true');
                                    option.attr('style', 'color:red');
                                }
                                comboBox.append(option)
                            });
                            $('.loaderContainer').css('display', 'none');
                        }
                    });
                });
                
            });
            
    
    
        </script>
@endsection
