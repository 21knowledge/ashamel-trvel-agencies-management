<div class="modal fade" tabindex="-1" role="dialog" id="booking_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content enquiry_form_modal_form">
            <div class="modal-header">
                <h5 class="modal-title">{{__("Booking")}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <?php
                //$rows=$datas["rows"][2]["title"];
                $rows=$datas["rows"];

        function OnSelectionChange() {
            echo("OK IT WORKS");
        }
    ?>

                <form method="post" action="{{url('admin/module/report/booking/bulkEdit')}}" class="filter-form filter-form-left d-flex justify-content-start">
                    @csrf
                    <select name="action" class="form-control" id="myDropDown">
                        <option value="">{{__("-- Tours--")}}</option>
                        @if(!empty($rows))
                            @foreach($rows as $row)
                                <option value="{{$row->id}}">{{__(' :name',['name'=>booking_status_to_text($row->title)])}}</option>
                            @endforeach
                        @endif
                    </select>

                </form>
                <div class="mt-2" id="booking-form">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-lg-9">
                 <h2>test </h2>
                                @include('Tour::frontend.layouts.details.vendor')
                                @include('Tour::frontend.layouts.details.tour-form-book')
                            </div>
                        </div>

                    </div>
                </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                <script>
                    $(document).ready(function(){
                        $("#booking-form").hide();
                        $('#myDropDown').change(function(){
                            let inputValue = $(this).val()
                            console.log("myDropDowng",inputValue)
                            $("#booking-form").show();
                        });
                    });



                </script>

                    <script>

                            var bravo_booking_i18n = {
                            no_date_select:'{{__('Please select Start date')}}',
                            no_guest_select:'{{__('Please select at least one guest')}}',
                            load_dates_url:'{{route('tour.vendor.availability.loadDates')}}',
                            name_required:'{{ __("Name is Required") }}',
                            email_required:'{{ __("Email is Required") }}',
                        };
                    </script>
                    <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
                    <script type="text/javascript" src="{{ asset("libs/fotorama/fotorama.js") }}"></script>
                    <script type="text/javascript" src="{{ asset("libs/sticky/jquery.sticky.js") }}"></script>
                    <script type="text/javascript" src="{{ asset('module/tour/js/single-tour.js?_ver='.config('app.version')) }}"></script>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                <button type="button" class="btn btn-primary btn-submit-enquiry">{{__("Send now")}}
                    <i class="fa icon-loading fa-spinner fa-spin fa-fw" style="display: none"></i>
                </button>
            </div>
        </div>
    </div>
</div>

