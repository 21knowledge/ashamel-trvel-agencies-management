<div class="form-checkout" id="form-checkout" >
    <input type="hidden" name="code" value="{{$booking->code}}">
    @for ($number = 1 ;$number <= $booking->total_guests ; $number++)
    <div class="form-section">
        <div class="row">

            <div class="row boxed">
                <div class="col-md-3">
                    <div class="form-group">
                        <h3 class="client">{{__("Guest # ")}}{{$number}}</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    

                    <div class="form-group">
                        <select name="country" currIndex="{{$number}}"  id="hotels_{{$number}}" class="form-control hotels">
                            <option value="">{{__('-- Hotel --')}}</option>
                            @foreach($hotels as $key=>$value)
                                <option @if(($user->country ?? '' ) == $hotels[$key]['id']) selected @endif value="{{$hotels[$key]['id']}}">{{$hotels[$key]['title']}}</option>
                             @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select name="country" currIndex="{{$number}}" id="rooms_{{$number}}"  class="form-control rooms">
                            <option value="">{{__('-- Room Type --')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("First Name")}} <span class="required">*</span></label>
                    <input type="text" placeholder="{{__("First Name")}}" class="form-control" value="{{$user->first_name ?? ''}}" name="first_name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("Last Name")}} <span class="required">*</span></label>
                    <input type="text" placeholder="{{__("Last Name")}}" class="form-control" value="{{$user->last_name ?? ''}}" name="last_name">
                </div>
            </div>
            <div class="col-md-6 field-email">
                <div class="form-group">
                    <label >{{__("Email")}} </label>
                    <input type="email" placeholder="{{__("email@domain.com")}}" id="phonetxt"  class="form-control" value="{{$user->email ?? ''}}" name="email">
                </div>
            </div>
            <div class="col-md-6 field-address-line-2">
                <div class="form-group">
                    <label >{{__("CIN/Passport")}} <span class="required">*</span></label>
                    <input type="text" placeholder="{{__("CIN/Passport")}}" class="form-control" value="{{$user->address2 ?? ''}}" name="address_line_2">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("Phone")}} <span class="required">*</span></label>
                    <input type="email" placeholder="{{__("Your Phone")}}" id="phonetxt" class="form-control" value="{{$user->phone ?? ''}}" name="phone">
                </div>
            </div>
            <div class="col-md-6 field-address-line-1">
                <div class="form-group">
                    <label >{{__("Address line 1")}} </label>
                    <input type="text" placeholder="{{__("Address line 1")}}" id="adressetxt"  class="form-control" value="{{$user->address ?? ''}}" name="address_line_1">
                </div>
            </div>
            <div class="col-md-4 field-city">
                <div class="form-group">
                    <label >{{__("City")}} </label>
                    <input type="text" class="form-control" value="{{$user->city ?? ''}}" id="citytxt"  name="city" placeholder="{{__("Your City")}}">
                </div>
            </div>
            {{-- <div class="col-md-6 field-state">
                <div class="form-group">
                    <label >{{__("State/Province/Region")}} </label>
                    <input type="text" class="form-control" value="{{$user->state ?? ''}}" name="state" placeholder="{{__("State/Province/Region")}}">
                </div>
            </div> --}}
            <div class="col-md-4 field-zip-code">
                <div class="form-group">
                    <label >{{__("ZIP code/Postal code")}} </label>
                    <input type="text" class="form-control" value="{{$user->zip_code ?? ''}}" id="zipcodetxt"  name="zip_code" placeholder="{{__("ZIP code/Postal code")}}">
                </div>
            </div>
            <div class="col-md-4 field-country">
                <div class="form-group">
                    <label >{{__("Country")}} </label>
                    <select name="country" id="countrytxt"  class="form-control">
                        <option value="">{{__('-- Select --')}}</option>
                        @foreach(get_country_lists() as $id=>$name)
                            <option @if(($user->country ?? '' ) == $id) selected @endif value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- <div class="col-md-12">
                <label >{{__("Special Requirements")}} </label>
                <textarea name="customer_notes" cols="30" rows="6" class="form-control" placeholder="{{__('Special Requirements')}}"></textarea>
            </div> --}}
        </div>
    </div>
    @endfor

    
    @include ('Booking::frontend/booking/checkout-deposit')
    @include ($service->checkout_form_payment_file ?? 'Booking::frontend/booking/checkout-payment')

    @php
    $term_conditions = setting_item('booking_term_conditions');
    @endphp

    <div class="form-group">
        <label class="term-conditions-checkbox">
            <input type="checkbox" name="term_conditions"> {{__('I have read and accept the')}}  <a target="_blank" href="{{get_page_url($term_conditions)}}">{{__('terms and conditions')}}</a>
        </label>
    </div>
    @if(setting_item("booking_enable_recaptcha"))
        <div class="form-group">
            {{recaptcha_field('booking')}}
        </div>
    @endif
    <div class="html_before_actions"></div>

    <p class="alert-text mt10" v-show=" message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></p>

    <div class="form-actions">
        <button class="btn btn-danger" @click="doCheckout">{{__('Submit')}}
            <i class="fa fa-spin fa-spinner" v-show="onSubmit"></i>
        </button>
    </div>
</div>
