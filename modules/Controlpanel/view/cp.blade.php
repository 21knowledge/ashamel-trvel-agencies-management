<!DOCTYPE html>
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <title>21Travel - The Booking System</title>
    <link rel="icon" type="image/png" href="{{url('icon/favicon.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="{{url('landing')}}/bs/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{url('landing')}}/owlcarousel/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="{{url('landing')}}/css/main.css"/>
    <link rel="icon" type="image/png" href="{{url('images/favicon.png')}}" />
</head>
<body>
{!! setting_item('body_scripts') !!}


<div class="header parallax">
    <div id="main-menu" class="sticky">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>
                    <!-- <a href="{{url(app_get_locale(false,'/'))}}" class="bravo-logo">
                    @php
                        $logo_id = setting_item("logo_id");
                        if(!empty($row->custom_logo)){
                            $logo_id = $row->custom_logo;
                        }
                    @endphp
                    @if($logo_id)
                        <?php $logo = get_file_url($logo_id,'full') ?>
                        <img src="{{$logo}}" alt="{{setting_item("site_title")}}">
                    @endif
                </a> -->
                    </h1>
                </div>
                <div class="col-md-8">
                    <!-- <div class="dropdown dropdown-main-menu">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="https://www.facebook.com/bookingcore/" target="_blank">Support</a>
                            <a target="_blank" class="dropdown-item" href="http://docs.bookingcore.org">Documentation</a>
                            <a target="_blank" href="{{config('landing.item_url')}}" class="dropdown-item btn-buynow">BUY NOW</a></li>
                        </div>
                    </div>
                    <ul class="menu">
                        <li>
                            <a target="_blank" href="{{config('landing.item_url')}}" class="btn-buynow">BUY NOW</a></li>
                        <li><a target="_blank" href="https://www.facebook.com/bookingcore/">Support</a></li>
                        <li><a target="_blank" href="http://docs.bookingcore.org">Documentation</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row ld-full-height">
            <div class="col-lg-12" style="text-align:center">
                <h2 class="heading">
                    <span>A'shamel</span>
                    Travel Agency Central<br/>
                    <h4>Manage every inch of your angency</h4>
                </h2>
                <div class="box">
                <h3 class="heading">
                    <span>Login</span>
                </h3>
                @include('Layout::auth/login-form')
            </div>
            </div>
        </div>
    </div>
</div>

<style>
    .box {
    border: 7px solid #cad7f0;
    padding: 22px;
    border-radius: 29px;
    background: #f6f8ff5e;
    width: 450px;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
</style>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115740936-4"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-115740936-4');
</script>
<script src="{{url('landing')}}/js/jquery.min.js"></script>
<script src="{{url('landing')}}/js/bootstrap.min.js"></script>
<script src="{{url('landing')}}/owlcarousel/owl.carousel.min.js"></script>
<script src="{{url('landing')}}/js/jquery.marquee.min.js"></script>
<script src="{{url('landing')}}/js/scrollreveal.js"></script>
<script src="{{url('landing')}}/js/jquery.matchHeight.js"></script>
<script src="{{url('landing')}}/js/main.js"></script>
</body>
</html>
