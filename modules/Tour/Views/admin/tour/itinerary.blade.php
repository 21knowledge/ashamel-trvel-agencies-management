<div class="form-group-item">
    <h3 class="control-label">{{__('Tour Program')}}</h3>
    <div class="g-items-header">
        <div class="row">
            <div class="col-md-2 text-left">{{__("Image")}}</div>
            <div class="col-md-3 text-left">{{__("Title - Desc")}}</div>
            <div class="col-md-3 text-left">{{__("Hotel")}}</div>
            <div class="col-md-3">{{__('Content')}}</div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <div class="g-items">
        @if(!empty($translation->itinerary))
            @php if(!is_array($translation->itinerary)) $translation->itinerary = json_decode($translation->itinerary); @endphp
            @foreach($translation->itinerary as $key=>$itinerary)

                <div class="item" data-number="{{$key}}">
                    <div class="row">
                        <div class="col-md-2">
                            {!! \Modules\Media\Helpers\FileHelper::fieldUpload('itinerary['.$key.'][image_id]',$itinerary['image_id'] ?? '') !!}
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="itinerary[{{$key}}][title]" class="form-control" value="{{$itinerary['title'] ?? ""}}" placeholder="{{__('Title: Day 1')}}">
                            <input type="text" name="itinerary[{{$key}}][desc]" class="form-control" value="{{$itinerary['desc'] ?? ""}}" placeholder="{{__('Desc: TP. HCM City')}}">
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="itinerary[{{$key}}][hotel]" onchange="loadRomms(this)" currIndex="{{$key}}"  id="hotels_{{$key}}" class="form-control hotels">
                                    <option value="">{{__('-- Hotel --')}}</option>
                                    @foreach($hotels as $key2=>$value)
                                        <option @if(($itinerary['hotel'] ?? '' ) == $hotels[$key2]['id']) selected @endif value="{{$hotels[$key2]['id']}}">{{$hotels[$key2]['title']}}</option>
                                     @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <select name="itinerary[{{$key}}][room]" currIndex="{{$key}}" id="rooms{{$key}}"  class="form-control rooms">
                                    <option value="">{{__('-- Room Type --')}}</option>
                                    @if (count($rooms)>0)
                                        @foreach($rooms[$key] as $key2=>$value)
                                            <option @if(($itinerary['room'] ?? '' ) == $value['id']) selected @endif value="{{$value['id']}}">{{$value['title']}}  F.Pls: {{$value['leftPlaces']}}/{{$value['totalPlaces']}}</option>
                                        @endforeach
                                    @endif
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <textarea name="itinerary[{{$key}}][content]" class="form-control full-h" placeholder="...">@if(!empty($itinerary['content'])){{$itinerary['content']}}@endif</textarea>
                        </div>
                        <div class="col-md-1">
                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="text-right">
            <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> {{__('Add item')}}</span>
    </div>
    <div class="g-more hide">
        <div class="item" data-number="__number__">
            <div class="row">
                <div class="col-md-2">
                    {!! \Modules\Media\Helpers\FileHelper::fieldUpload('itinerary[__number__][image_id]','','__name__') !!}
                </div>
                <div class="col-md-3">
                    <input type="text" __name__="itinerary[__number__][title]" class="form-control" placeholder="{{__('Title: Day 1')}}">
                    <input type="text" __name__="itinerary[__number__][desc]" class="form-control" placeholder="{{__('Desc: TP. HCM City')}}">
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <select __name__="itinerary[__number__][hotel]" onchange="loadRomms(this)" currIndex="__number__"  id="hotels__number__" class="form-control hotels">
                            <option value="">{{__('-- Hotel --')}}</option>
                            @foreach($hotels as $key2=>$value)
                                <option @if(($user->country ?? '' ) == $hotels[$key2]['id']) selected @endif value="{{$hotels[$key2]['id']}}">{{$hotels[$key2]['title']}}</option>
                             @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select __name__="itinerary[__number__][room]" currIndex="__number__" id="rooms__number__"  class="form-control rooms">
                            <option value="">{{__('-- Room Type --')}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <textarea __name__="itinerary[__number__][content]" class="form-control full-h" placeholder="..."></textarea>
                </div>
                <div class="col-md-1">
                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>